# README #

A Tungsten module for integrating with MailChimp's API. This requires access to the MailChimp account which can be granted
by the client.

## Granting Access
The webpage below explains how a client can grant you access to their MailChimp account.

https://mailchimp.com/help/manage-user-levels-in-your-account/

## Obtaining an API Key
You must get an API Key from within the MailChimp interface before being permitted to connect.

1. Open http://mailchimp.com
1. Log in and ensure you are viewing the client's account
1. Click on the account name in the top right, then click on `Account`
1. Click on `Extras`, then `API Keys`
1. Click on `Create an API Key`
1. Copy that API Key

## Setup
1. Log into Tungsten
1. Install the module in `System` 
1. Go to `/admin/mailchimp/do/settings/`
1. Paste the API Key and update the settings 
1. You should now see a successful connection, select a default list

## Adding a Subscription Form
1. Open the `Pages` module
1. Open the Page Builder for the relevant page
1. Add the `MailChimp Subscribe Form`
1. Style Accordingly, extending the class with overrides if necessary