<?php

Class TMm_MailChimpPostList extends TCm_ModelList 
{
	
	public function __construct()
	{
		parent::__construct('TMm_MailChimpPost');
	}

	public function processFilterList($filter_values)
	{
		$where_clauses = array();
		$db_values = array();

		if($filter_values['search'] != '')
		{
			$where_clauses[] = "`title` LIKE :search";
			$db_values[':search'] = '%'.$filter_values['search'].'%';
			
		}

		if ( $filter_values['mail_chimp_list_id'] > 0 )
		{
			$where_clauses[] = '`mail_chimp_list_id` = :mail_chimp_list_id';
			$db_values[':mail_chimp_list_id'] = $filter_values['mail_chimp_list_id'];
		}

		$query = 'SELECT * FROM `mail_chimp_posts`';

		if ( !empty( $where_clauses ) ) 
		{
			$query .= ' WHERE ' . implode(' AND ', $where_clauses );	
		}

		$query .= ' ORDER BY `send_time` DESC';

		$result = $this->DB_Prep_Exec( $query, $db_values );
		return $this->returnValueForFilterResult( $filter_values, $result );

	}
}
?>