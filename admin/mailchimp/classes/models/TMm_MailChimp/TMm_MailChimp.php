<?php
// This class extends the MailChimp class written by DrewM
include_once($_SERVER['DOCUMENT_ROOT'].'/admin/mailchimp/classes/models/MailChimp_Library/src/MailChimp.php');

use \DrewM\MailChimp\MailChimp; // use the namespace
 
/**
 * Class TMm_MailChimp
 */
class TMm_MailChimp extends MailChimp
{
	use TSt_ConsoleAccess, TCt_DatabaseAccess, TCt_FactoryInit;

	protected $has_connection = false;
	protected $account_id = false;
	protected $account_name = false;

	protected $lists = false;

	// protected $default_list_id = null;
	protected $post_list = null;
	protected $post_list_params = null;

	/**
	 * TMm_MailChimp constructor.
	 */
	public function __construct()
	{
		$api_key = TC_getModuleConfig('mailchimp', 'api_key');
		try
		{
			parent::__construct($api_key);
			$response = $this->get("/");
			if(isset($response['account_id']))
			{
				$this->account_id = $response['account_id'];
				$this->account_name = $response['account_name'];

			}


		}
		catch(Exception $e)
		{
			$this->addConsoleDebug('MailChimp API Key invalid');
		}

		// $this->default_list_id = TC_getModuleConfig('mailchimp', 'default_list_id');
		
	}

	/**
	 * Returns if a connection is established with the server
	 * @return bool
	 *
	 */
	public function connectionEstablished()
	{
		return $this->account_id !== false;
	}

	/**
	 * Returns the account ID
	 * @return int
	 */
	public function accountID()
	{
		return $this->account_id;
	}

	/**
	 * Returns the account ID
	 * @return string
	 */
	public function accountName()
	{
		return $this->account_name;
	}

	/**
	 * Function to check if a subscriber exists in a list
	 * @param string $email
	 * @param string $list_id
	 * @return bool
	 */
	public function subscriberStatusForList($email, $list_id)
	{
		$response = $this->get('lists/'.$list_id.'/members/'.$this->subscriberHash($email));
	
		if($response['status'] == 404)
		{
			return false;
		}
		return $response['status'];
		
	}
	
	// NOTE, TWO MODIFICATIONS HAD TO HAPPEN
	// makeRequest() and $this->api_endoint where both changed to protected
	 /**
     * Performs the underlying HTTP request. Not very exciting.
     * @param  string $http_verb The HTTP verb to use: get, post, put, patch, delete
     * @param  string $method The API method to be called
     * @param  array $args Assoc array of parameters to be passed
     * @param int $timeout
     * @return array|false Assoc array of decoded result
     * @throws \Exception
     */
    protected function makeRequest($http_verb, $method, $args = array(), $timeout = 10)
    {
		$message = strtoupper($http_verb).' : '.$this->api_endpoint . '/' . $method;

		$response = array();
		$response['args'] = $args;

		$response['response'] = parent::makeRequest($http_verb, $method, $args, $timeout);
		$this->addConsoleMessageWithType($message, TSm_ConsoleAPICall, $is_error = false, $response);
		return $response['response'];
	}
	//*/


	/**
	 * Returns the last time a request was sent
	 * @return bool|mixed
	 */
    public function getLastSendTime()
    {
    	$res = $this->DB_Prep_Exec( 'SELECT send_time FROM mail_chimp_posts ORDER BY send_time DESC LIMIT 1' );
    	if ( $res )
    	{
    		return $res->fetch();
    	}

    	return false;
    }

	/**
	 * Removes attributes from a DOMDocument
	 * @param DOMDocument $domd
	 * @param $element
	 * @param $attribute
	 * @return DOMDocument
	 */
    public function removeAttributes( DOMDocument $domd, $element, $attribute )
    {
		$domx = new DOMXPath($domd);
		$items = $domx->query("//" . $element . "[@" . $attribute ."]");

		foreach($items as $item) {
		  $item->removeAttribute($attribute);
		}

		return $domd;
    }

	/**
	 * Cleans the HTML
	 * @param string $html
	 * @return string
	 */
    public function getHTMLBody($html)
    {

    	$doc = new DOMDocument();
    	$doc->loadHTML($html);
		$body = $doc->getElementsByTagName('body');
		if ( $body && $body->length  > 0 )
		{
			$body = $body->item(0);
			$body->removeAttribute("style");

			$string = $doc->saveHTML($body);
			$string = str_ireplace('<body','<div', $string);
			$string = str_ireplace('</body','</div', $string);

			return $string;

		}
		return $html; // just in case return it all
////
////
//    	// elements to remove - these names are probably somewhat template dependent
//    	$preheader = $doc->getElementById('templatePreheader');
//    	$templateFooter = $doc->getElementById('templateFooter');
//    	$preheader->parentNode->removeChild($preheader);
//    	$templateFooter->parentNode->removeChild($templateFooter);
////
//    	$doc = $this->removeAttributes( $doc, 'table', 'style' );
////    	$doc = $this->removeAttributes( $doc, 'td', 'style' );
//    	$doc = $this->removeAttributes( $doc, 'img', 'width' );
//
//    	// only return the first table.
//    	$first_table = $doc->getElementsByTagName('table');
//
//		$first_table = $first_table->item(0);
//		return $doc->savehtml($first_table);
//
    }

	/**
	 * Cleans the HTML
	 * @param string $html
	 * @return string
	 */
	public function getHTMLEmbeddedStyles($html)
	{
		$doc = new DOMDocument();
		$doc->loadHTML($html);
		$tag = $doc->getElementsByTagName('style');
		if ( $tag && $tag->length  > 0 )
		{
			return $tag->item(0)->nodeValue;
			//return $doc->saveHTML($tag);
		}
		return ''; // just in case return nothing
	}

	/**
	 * Syncs the relevant info
	 */
	public function sync()
	{
		$num_added = 0;
		$last_send_time = $this->getLastSendTime();
		$since_send_time = ( $last_send_time ) ? $last_send_time['send_time'] : '';

		$lists = $this->get( 'lists/' );
		$mail_chimp_lists_by_api_list_id = array();

		if ( $lists )
		{
			foreach( $lists['lists'] as $list )
			{
				$params = array(
							':api_list_id'	=>	$list['id'],
							':name'		=>	$list['name']
							);
				if ( $mail_chimp_list = $this->findListByAPIListId( $list['id'] ) )
				{
					// update
					$params = array_merge(
								array(	':list_id'	=>	$mail_chimp_list->id() ),
								$params
						);
					$mail_chimp_list = $mail_chimp_list->updateWithValues( $params );
				}
				else
				{
					// create
					$mail_chimp_list = TMm_MailChimpList::createWithValues($params);
				}
				$mail_chimp_lists_by_api_list_id[$list['id']] = $mail_chimp_list;
			}
		}

		// $res = $this->get('campaigns/', array( 'list_id' => $this->default_list_id, 'since_send_time'	=>	$since_send_time ) );
		$res = $this->get('campaigns/', array( 'since_send_time'	=>	$since_send_time ) );

		if ( $res )
		{
			foreach ($res['campaigns'] as $campaign) {

				$clean_html = '';
				$style_text = '';
				$details = $this->get('campaigns/' . $campaign['id'] . '/content' );

				if ( !empty( $details['html'] ) )
				{
					$clean_html = $this->getHTMLBody( $details['html'] );
					$style_text = $this->getHTMLEmbeddedStyles($details['html']);
				}

				if ( !empty( $details['plain_text'] ) )
				{
					$details['plain_text'] = str_ireplace("*|MC_PREVIEW_TEXT|*",'', $details['plain_text']);
					$details['plain_text'] = str_ireplace("*",'', $details['plain_text']);
				}

				$params = array(
					':title'				=>	$campaign['settings']['subject_line'],
					':raw_html'				=>	( !empty($details['html']) ? $details['html'] : ''),
					':clean_html'			=>	$clean_html,
					':style_text'			=>	$style_text,
					':plain_text'			=>	( !empty($details['plain_text']) ? $details['plain_text'] : ''),
					':original_id'			=>	$campaign['id'],
					':send_time'			=>	$campaign['send_time'],
					':mail_chimp_list_id'	=>	$mail_chimp_lists_by_api_list_id[$campaign['recipients']['list_id']]->id()
					);

				
				// use TCm_Model's createWithValues()
				// matches paramter name to column names
				// retuns an object that you used to call the method
				// auto-fills date_added()
				$mail_chimp_post = TMm_MailChimpPost::createWithValues($params);
				
				if ( $mail_chimp_post )
				{
					$num_added++;
				}


			}
		}
		TC_Message( $num_added . ' newsletters synced.');
		// INSERT SYNC CODE HERE
		$this->addConsoleDebug('sync() happened');
	}

	/**
	 * Returns the number of posts
	 * @return bool
	 */
	public function getPostsCount()
	{
		$res =$this->DB_Prep_Exec( 'SELECT count(*) AS cnt FROM mail_chimp_posts WHERE is_visible = 1' );

		if ( $res ) 
		{
			$row = $res->fetch();
			return $row['cnt'];
		}

		return false;
	}

	/**
	 * Returns the list of posts
	 *
	 * @param $mail_chimp_list_id
	 * @param int $per_page
	 * @param int $page
	 * @return null
	 */
	public function getPostsList($mail_chimp_list_id, $per_page = 10, $page = 1 )
	{
		if ( empty($this->post_list) || $this->post_list_params !== array( 'per_page' => $per_page, 'page' => $page ) )
		{
			$this->post_list_params = array( 'per_page' => $per_page, 'page' => $page );

			
			$res = $this->DB_Prep_Exec( sprintf('SELECT `post_id` 
														FROM `mail_chimp_posts`
														INNER JOIN `mail_chimp_lists` ON `mail_chimp_posts`.`mail_chimp_list_id` = `mail_chimp_lists`.`mail_chimp_list_id` 
														WHERE `mail_chimp_posts`.`is_visible` = 1 AND `mail_chimp_lists`.`list_id` = %d
														ORDER BY send_time DESC LIMIT %d, %d', 
										$mail_chimp_list_id, (($page - 1) * $per_page), $per_page) ); // note: can't use PDO::prepare() with LIMIT

			if ( $res )
			{
				while( $row = $res->fetch() )
				{
					$this->post_list[] = TMm_MailChimpPost::init($row['post_id'] );
				}
			}
		}
		
	
		return $this->post_list;
	}

	/**
	 * Returns the list of lists
	 * @return object[]
	 */
	public function lists()
	{
		if($this->lists === false)
		{
			$response= $this->get( 'lists/' );
			$this->lists = $response['lists'];
		}

		return $this->lists;

	}

	/**
	 * @param string $api_list_id
	 * @return bool|TCu_Item
	 */
	public function findListByAPIListId( $api_list_id )
	{
		$res = $this->DB_Prep_Exec( 'SELECT list_id FROM mail_chimp_lists WHERE api_list_id = :api_list_id LIMIT 1',
									array( 
										':api_list_id'	=>	$api_list_id
										)
									); 

		if ( $res && $row = $res->fetch() )
		{

			return TC_initClass( 'TMm_MailChimpList', $row['list_id'] );
		}

		return false;
	}
}
?>