<?php
/*	CLASS		:	TMm_MailChimpPost
	DESCRIPTION	:	Class that represents a mail chimp post

 */
class TMm_MailChimpPost extends TCm_Model
{
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'post_id'; // [string] = The id column for this class
	public static $table_name = 'mail_chimp_posts'; // [string] = The table for this class
	public static $model_title = 'Mail Chimp Post'; 
	public static $primary_table_sort = 'send_time DESC';
	
	/* 	FUNCTION	: 	__construct
		DESCRIPTION	:	Constructor which accepts the user_id
		PARAMS		:	post_id [int] = The ID for the group
		RESULT		:	
		ADDED		:	
	 */
	public function __construct($post_id)
	{
		parent::__construct($post_id);

		// instantiate group from table
		$this->setPropertiesFromTable();
		if(!$this->exists) { return null; };
		
	}

	public function hasHTML()
	{
		return !empty( $this->clean_html );
	}

	/**
	 * Returns the title
	 * @return string
	 */
	public function title()
	{
		return $this->title;
	}

	/**
	 * Returns the date and time sent
	 * @return mixed
	 */
	public function dateSent()
	{
		return $this->send_time;
	}

	/**
	 * Returns the description stripped of tags and trimmed to X characters
	 * @param int $num_characters THe number of characters to trim to
	 * @return string
	 */

	public function contentStrippedAndTrimmed($num_characters)
	{
		$content = $this->contentStripped();
		if(strlen($content) - 20 > $num_characters) // 20 character buffer
		{
			return substr($content, 0, $num_characters).'…';
		}

		return $content;
	}





	/**
	 * Returns the date sent formatted
	 * @param string $format
	 * @return false|string
	 */
	public function dateSentFormatted($format = 'F d, Y \a\t g:ia')
	{
		return date($format, strtotime($this->send_time));
	
	}

	/**
	 * Returns the content for this post
	 * @return string
	 */
	public function content()
	{
		if ( !empty( $this->clean_html ) )
		{
			return $this->clean_html;	
		}
		
		return $this->plain_text;
	}

	public function id()
	{
		return $this->post_id;
	}

	public function plain()
	{
		return $this->plain_text;
	}



	/**
	 * Returns the preview text, trimmed to x characthers
	 * @param int $num_characters THe number of characters to trim to
	 * @return mixed
	 */
	public function excerpt($num_characters = 300)
	{
		$content =  str_ireplace("\n",' ',$this->plain_text);
		if(strlen($content) - 20 > $num_characters) // 20 character buffer
		{
			return substr($content, 0, $num_characters).'…';
		}

		return $content;
	}





	public function listId()
	{

		return $this->mail_chimp_list_id;
	}

	public function listName()
	{
		if ( $this->listId() )
		{
			$mail_chimp_list = TC_initClass( 'TMm_MailChimpList', $this->listId());

			if ( $mail_chimp_list )
			{
				return $mail_chimp_list->name();
			}	
		}
		

		return false;
	}

	public function getNext()
	{
		$res = $this->DB_Prep_Exec( 'SELECT * FROM mail_chimp_posts WHERE post_id = (SELECT MIN(post_id) FROM mail_chimp_posts WHERE is_visible = 1 AND post_id > :post_id)', 
								array( ':post_id'	=>	$this->id )
								);

		if ( $res->rowCount() > 0 )
		{
			return TMm_MailChimpPost::init($res->fetch());
		}

		return false;
	}

	public function getPrev()
	{
		$res = $this->DB_Prep_Exec( 'SELECT * FROM mail_chimp_posts WHERE post_id = (SELECT MAX(post_id) FROM mail_chimp_posts WHERE is_visible = 1 AND post_id < :post_id)', 
								array( ':post_id'	=>	$this->id )
								);

		if ( $res->rowCount() > 0 )
		{
			return TMm_MailChimpPost::init($res->fetch());
		}

		return false;
	}
	
	public function isVisible()
	{
		return $this->is_visible;
	}

	public function toggleIsVisible()
	{
		// handles the DB update as well as the internal class value for this instance
		$this->updateDatabaseValue('is_visible', !$this->is_visible);
	}



	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////

	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema(): array
	{
		return parent::schema() + [
				'title' => [
					'type'          => 'text',
					'nullable'      => false,
				],
				'raw_html' => [
					'type'          => 'longtext',
					'nullable'      => false,
				],
				'clean_html' => [
					'type'          => 'longtext',
					'nullable'      => true,
				],
				'style_text' => [
					'type'          => 'longtext',
					'nullable'      => false,
				],
				'is_visible' => [
					'type'          => 'boolean DEFAULT 1',
					'nullable'      => false,
				],
				'plain_text' => [
					'type'          => 'longtext',
					'nullable'      => true,
				],
				'original_id' => [
					'type'          => 'text',
					'nullable'      => false,
				],
				'send_time' => [
					'type'          => 'datetime DEFAULT NULL',
					'nullable'      => true,
				],
				'mail_chimp_list_id' => [
					'type'          => 'TMm_MailChimpList',
					'nullable'      => false,

					'foreign_key'   => [
						'model_name'    => 'TMm_MailChimpList',
						'delete'        => 'SET DEFAULT'
					],
				],

			];


	}
}
?>