<?php 

class TMm_MailChimpList extends TCm_Model
{

	protected $posts = false;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'list_id'; // [string] = The id column for this class
	public static $table_name = 'mail_chimp_lists'; // [string] = The table for this class
	public static $model_title = 'Mail Chimp Lists'; 
	public static $primary_table_sort = 'name ASC';
	public static $lists = array();

	public function __construct($id = false)
	{

		parent::__construct($id);

		// instantiate group from table
		$this->setPropertiesFromTable();
		if(!$this->exists) { return null; };

	}


	public function id()
	{
		return $this->id;
	}

	public function APIListId()
	{
		return $this->api_list_id;
	}

	public function name()
	{
		return $this->name;
	}

	/**
	 * Returns the posts associated with this list
	 * @return TMm_MailChimpPost[]
	 */
	public function posts()
	{
		if($this->posts === false)
		{
			$this->posts = array();
			$query = "SELECT * FROM mail_chimp_posts WHERE mail_chimp_list_id = :id ORDER BY send_time DESC";
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			while($row = $result->fetch())
			{
				$this->posts[] = TMm_MailChimpPost::init($row);
			}
		}

		return $this->posts;
	}

	public function updateWithValues( $values, $bind_array_params = array())
	{
		$set_sql = array();

		if ( count( $values ) == 0 )
		{
			return false;
		}

		foreach ($values as $field => $value) 
		{
			$field_name = substr($field, 1);
			$set_sql[] = "`$field_name` = $field";
		}

		$set_sql = implode(', ', $set_sql);
		$sql = sprintf( 'UPDATE `%s` SET %s WHERE `%s` = :%s', self::$table_name, $set_sql, self::$table_id_column, self::$table_id_column);
		$res = $this->DB_Prep_exec( $sql, $values );
		
		if ( $res )
		{
			$this->setPropertiesFromTable();
			return $this;
		}

		return false;
		
	}


	//////////////////////////////////////////////////////
	//
	// SCHEMA
	//
	//////////////////////////////////////////////////////

	/**
	 * Define the schema for this model
	 * @return array[]
	 */
	public static function schema(): array
	{
		return parent::schema() + [
				'api_list_id' => [
					'type'          => 'VARCHAR(255)',
					'nullable'      => true,
				],
				'name' => [
					'type'          => 'VARCHAR(255)',
					'nullable'      => true,
				],
			];

	}
}