<?php

/**
 * Trait TMt_MailChimpUser
 *
 * Apply this trait to an extension of a TMm_User class (or other classes that have methods for email(), firstName()
 * and lastName() to add the functionality related to syncing users to the system.
 * with
 * MailChimp.
 *
 *
 */
trait TMt_MailChimpUser
{
	/**
	 * Adds this user to MailChimp
	 * @param string|bool $list_id (Optional) Default false will use the default list_id in the settings set
	 * MailChimp module
	 */
	public function updateMailChimp($list_id = false)
	{
		if($list_id == false)
		{
			$list_id = TC_getModuleConfig('mailchimp', 'default_list_id');
		}

		$this->validateClass();

		/** @var TMm_MailChimp $mailchimp */
		$mailchimp = TC_initClass('TMm_MailChimp');

		$mailchimp->addPersonToList($this->email(), $list_id, $this->firstName(), $this->lastName(),
									$this->mailChimpInterests() );

	}

	public function validateClass()
	{
		if(!method_exists($this, 'email'))
		{
			TC_triggerError(get_called_class().' requires a method called "email()" in order to sync with MailChimp');
		}

		if(!method_exists($this, 'firstName'))
		{
			TC_triggerError(get_called_class().' requires a method called "firstName()" in order to sync with MailChimp');
		}

		if(!method_exists($this, 'lastName'))
		{
			TC_triggerError(get_called_class().' requires a method called "lastName()" in order to sync with MailChimp');
		}

	}

	/**
	 * Returns the list of interests for this user
	 * @return bool|array
	 */
	public function mailChimpInterests()
	{
		return false;
	}


}