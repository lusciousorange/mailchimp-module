<?php
/**
 * Class TMc_MailChimpController
 */
class TMc_MailChimpController extends TSc_ModuleController
{
	/**
	 * TMc_MailChimpController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

	}


	/**
	 * Defines the URLs for this controller
	 */
	public function defineURLTargets()
	{
		if(TC_getModuleConfig('mailchimp','sync_posts'))
		{
			// URL : /admin/mailchimp/ OR /admin/mailchimp/do/
			// Redirect to the List URL Target
			$item = TSm_ModuleURLTarget::init('');
			$item->setNextURLTarget('list');
			$this->addModuleURLTarget($item);


			// URL : /admin/mailchimp/do/list/
			// The list of all the Mail Chimp Posts
			$item = TSm_ModuleURLTarget::init('list');
			$item->setViewName('TMv_MailChimpPostList');
			$item->setTitle('Posts');
			$this->addModuleURLTarget($item);

			// URL : /admin/mailchimp/do/sync/
			// Runs the sync for MailChimp. Usefully until a web-hook is in place for testing
			// This URL target will
			// - instantiate a TMm_MailChimp object,
			// - call the sync() method on that object,
			// - return the user to the page where it was originally called from
			$item = TSm_ModuleURLTarget::init('sync');
			$item->setModelName('TMm_MailChimp');
			$item->setModelActionMethod('sync()');
			$item->setTitle('Sync');
			$item->setNextURLTarget('referrer');
			$item->setAsRightButton('fa-sync');
			$this->addModuleURLTarget($item);

			// URL : /admin/mailchimp/do/edit/
			// The form to edit a single post. Not sure what this would entail, but it's here
			$item = TSm_ModuleURLTarget::init('edit');
			$item->setViewName('TMv_MailChimpPostForm');
			$item->setModelName('TMm_MailChimpPost');
			$item->setModelInstanceRequired();
			$item->setTitleUsingModelMethod('title');
			$this->addModuleURLTarget($item);

			// URL : /admin/mailchimp/do/toggle-visible/<id>
			// Toggles the visibilty for a single MailChimp Post, returns to where it was called from.
			$item = TSm_ModuleURLTarget::init('toggle-visible');
			$item->setModelName('TMm_MailChimpPost');
			$item->setModelInstanceRequired();
			$item->setModelActionMethod('toggleIsVisible()');
			$item->setTitle('Toggle Visible');
			$item->setNextURLTarget('referrer');
			$this->addModuleURLTarget($item);
		}
		else
		{
			// URL : /admin/mailchimp/ OR /admin/mailchimp/do/
			// Redirect to the List URL Target
			$item = TSm_ModuleURLTarget::init('');
			$item->setNextURLTarget('settings');
			$this->addModuleURLTarget($item);

		}

	}
	
	
	

}
?>