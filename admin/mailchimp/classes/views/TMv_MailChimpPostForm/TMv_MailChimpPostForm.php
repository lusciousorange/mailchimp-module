<?php
/*	CLASS		:	TMv_MailChimpPostForm
	DESCRIPTION	:
	ADDED		:	
 */
class TMv_MailChimpPostForm extends TCv_FormWithModel
{
	/* 	FUNCTION	: 	__construct
		DESCRIPTION	:	Constructor 
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
	}
	
	/* 	FUNCTION	: 	configureFormElements
		DESCRIPTION	:	Function where your form items should be instantiated. This class must be extended in order to make use of this feature. Do not put the submit button in this function since it is automatically created when the form is generated.
		PARAMS		:	
		RESULT		:	[HTML]
		ADDED		:	
	 */
	public function configureFormElements()
	{
		// IF you need to be able to edit a post after it's in our system, here's where you would modify the values in the DB. 
		$title = new TCv_FormItem_TextField('title', 'Title');
		$title->setIsRequired();
		$this->attachView($title);

		$is_visible = new TCv_FormItem_Checkbox('is_visible', 'Visible');
		$is_visible->setHelpText( 'Uncheck to hide from site' );
		$this->attachView($is_visible);
	}
	
}