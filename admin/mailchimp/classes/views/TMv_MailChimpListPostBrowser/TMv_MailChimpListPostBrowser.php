<?php
class TMv_MailChimpListPostBrowser extends TCv_View
{
	protected $list_id = false;

	use TMt_PagesContentView;

	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function html()
	{
		$list = TMm_MailChimpList::init($this->list_id);
		$this->addConsoleDebugObject('list', $list);
		foreach($list->posts() as $post)
		{
			if($post->isVisible())
			{
				/** @var TMv_MailChimpPostPreview $view */
				$view = TMv_MailChimpPostPreview::init($post);
				//$view->hideExcerpt();
				$view->setViewURL('/news/view/');
				// TODO : Change this to be a setting in the content view that lets them choose a page to direct to
				// Done in a few spots on FFT, Camp 8
				$this->attachView($view);
			}
		}

		return parent::html();
	}




	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////



	public static function pageContent_ViewTitle() : string { return 'MailChimp List - Post Browser'; }
	public static function pageContent_IconCode() : string { return 'fa-envelope'; }
	public static function pageContent_ViewDescription() : string { return 'Displays a list of Mailchimp posts'; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	public function pageContent_EditorFormItems() : array
	{

		$form_items = array();
		$select_options = array();

		/** @var TMm_MailChimpListList $mailchimp_lists */
		$mailchimp_lists = TMm_MailChimpListList::init();
		$lists = $mailchimp_lists->models();

		/** @var TMm_MailChimpList $list */
		foreach ($lists as $list) 
		{
			$select_options[$list->id()] = $list->name();
		}

		$select = new TCv_FormItem_Select( 'list_id', 'Select List' );
		$select->setOptionsWithArray( $select_options );
		$form_items[] = $select;

		return $form_items;
	}	
		
}
?>