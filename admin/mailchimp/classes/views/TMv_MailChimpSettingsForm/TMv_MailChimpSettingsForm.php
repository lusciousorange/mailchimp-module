<?php
class TMv_MailChimpSettingsForm extends TSv_ModuleSettingsForm
{
	/**
	 * TMv_MailChimpSettingsForm constructor.
	 * @param TSm_Module $module
	 */
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}

	/**
	 *
	 */
	public function configureFormElements()
	{

		$api_key = new TCv_FormItem_TextField('api_key', 'API Key');
		$api_key->setHelpText('Your MailChimp API Key.');
		$this->attachView($api_key);

		/** @var TMm_MailChimp $mailchimp */
		$mailchimp = TMm_MailChimp::init();

		$connection = new TCv_FormItem_HTML('api_key_verification', 'Connection');
		if($mailchimp->connectionEstablished())
		{
			$connection->addText('<strong>Connection Established</strong>');
			$connection->addText('<br />'.$mailchimp->accountName().' ('.$mailchimp->accountID().')');

		}
		else
		{
			$connection->addText('Connection NOT Established');
		}
		$this->attachView($connection);

		if($mailchimp->connectionEstablished())
		{
			$field = new TCv_FormItem_Select('default_list_id', 'Default List');
			$field->addOption('','Select a List');
			foreach($mailchimp->lists() as $list_response)
			{
				$field->addOption($list_response['id'], $list_response['name'].' ('.$list_response['id'].')');
			}
			$this->attachView($field);
		}

		$field = new TCv_FormItem_Select('sync_posts','Sync Posts');
		$field->setHelpText('Indicate if posts are synced with MailChimp');
		$field->addOption('0','No – No syncing of posts is used');
		$field->addOption('1','Yes– Posts are synced');

		$this->attachView($field);


	}

	/**
	 * @return bool|TCv_View
	 */
	public function helpView()
	{
		$help_view = new TCv_View();
			
			$p = new TCv_View();
			$p->setTag('h2');
			$p->addText('These are the settings for your MailChimp integration.  ');
			$help_view->attachView($p);
			
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText('You will need an API Key in order to connect to MailChimp. If you aren\'t familiar with acquiring your API Key, please read the <a target="_blank" href="http://kb.mailchimp.com/integrations/api-integrations/about-api-keys">MailChimp article on generating API keys</a>. ');
			$help_view->attachView($p);
			
			
					
		return $help_view;
	}
	
}
?>