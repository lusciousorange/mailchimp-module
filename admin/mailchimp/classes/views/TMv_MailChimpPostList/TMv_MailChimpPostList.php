<?php
/*	CLASS		:	TMv_MailChimpPostList
	DESCRIPTION	:	This is the list of posts in our system. This view is also used for the public site as a page content view that shows the posts using a preview class view instead of the traditioanl views.
	ADDED		:	
 */
class TMv_MailChimpPostList extends TCv_SearchableModelList
{
	/* 	FUNCTION	: 	__construct
		DESCRIPTION	:	Constructor
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_MailChimpPost');
		$this->defineColumns();
		$this->setPagination(25);
		
	}
	
	/* 	FUNCTION	: 	defineColumns
		DESCRIPTION	:	Defines the columns for this list
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function defineColumns()
	{
		$title = new TCv_ListColumn('title');
		$title->setTitle('Title');
		$title->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($title);

		$column = new TCv_ListColumn('date_sent');
		$column->setTitle('Date Sent');
		$column->setContentUsingModelMethod('dateSent()');
		$this->addTCListColumn($column);

		$list = new TCv_ListColumn( 'api_list_id' );
		$list->setTitle( 'List' );
		$list->setContentUsingModelMethod('listName()');
		$this->addTCListColumn($list);
	
		$column = $this->controlButtonColumnWithListMethod('visibleColumn');
		$this->addTCListColumn($column);
	
		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
				
	}
	
	public function defineFilters()
	{
		parent::defineFilters();

		if(TC_getConfig('admin_mode'))
		{
			/** @var TMm_MailChimpListList $mailchimp_lists */
			$mailchimp_lists = TMm_MailChimpListList::init();
			$lists = $mailchimp_lists->models();


			//$mail_chimp_list = new TMm_MailChimpList();
			$lists_field = new TCv_FormItem_Select('mail_chimp_list_id', 'Lists');
			$lists_field->addOption('', 'All Lists');

			foreach($lists as $list)
			{
				$lists_field->addOption($list->id(), $list->name());
			}

			$this->addFilterFormItem($lists_field);
		}
	}
	
	
	
	/* 	FUNCTION	: 	titleColumn
		DESCRIPTION	:	
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function titleColumn($model)
	{
		if($model->userCanEdit())
		{
			$url_target = TC_URLTargetFromModule( 'mailchimp' , 'edit');
			$url_target->setModel($model);
			$link = TSv_ModuleURLTargetLink::init($url_target);
			return $link;
			
		}
		
		return $model->title();

	}
	
	/* 	FUNCTION	: 	visibleColumn
		DESCRIPTION	:	
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function visibleColumn($model)
	{
		
		// Account for the "eye' being enabled or disabled
		$icon_code = 'fa-eye';
		$faded_class = '';
		if(!$model->isVisible())
		{
			$icon_code .= '-slash';
			$faded_class = 'faded';
		}
		
		// This is the LONG VERSION of what that one line does in editIconColumn
		// It does the same thing, but I figured understanding would be helpful
		
		// Find the appropriate URL target and set the model
		$url_target = TC_URLTargetFromModule('mailchimp', 'toggle-visible');
		$url_target->setModel($model);
		
		// Pass that URL into a URL Target Link
		// Then set properites for that link
		$link = TSv_ModuleURLTargetLink::init($url_target);
		$link->setIconClassName($icon_code);
		$link->addClass('list_control_button');
		$link->addClass($faded_class);
		$link->addOverrideText('');
		
		return $link;
		
		
	}

	/* 	FUNCTION	: 	editIconColumn
		DESCRIPTION	:	
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'edit', 'fa-pencil');
		
	}

		
}
?>