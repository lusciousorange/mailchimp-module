<?php
class TMv_MailChimpPostView extends TCv_View
{
	use TMt_PagesContentView;

	/** @var bool|TMm_MailChimpPost $mail_chimp_post */
	protected $mail_chimp_post = false;
	
	/* 	FUNCTION	: 	__construct
		DESCRIPTION	:	Constructor which accepts the module
		PARAMS		:	mail_chimp_post [TMm_MailChimpPost] 
		RESULT		:	
		ADDED		:	
	 */
	public function __construct($mail_chimp_post)
	{	
		parent::__construct();
		$this->addClassCSSFile('TMv_MailChimpPostView');
		$this->mail_chimp_post = $mail_chimp_post;
	}
	
	
	/* 	FUNCTION	: 	html
		DESCRIPTION	:	
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function html()
	{	
		$page_headings = new TCv_View('page_headings');
		$page_headings->addClass('content_width_container');

		if(!$this->mail_chimp_post->isVisible())
		{
			return parent::html();
		}

		if ( $this->mail_chimp_post->hasHTML() )
		{
			$this->addText($this->mail_chimp_post->content());	
		}
		else
		{
			// if the html is missing (or failed to clean), display plain text.
			$pre = new TCv_View();
			$pre->setTag('pre');
			$pre->addText( $this->mail_chimp_post->plain() );
			$this->attachView( $pre );
		}

		$prev_next_div = new TCv_View('prev_next');

		$prev_post = $this->mail_chimp_post->getPrev();		
		$next_post = $this->mail_chimp_post->getNext();

		if ( $prev_post ){
			$prev_link = new TCv_Link();
			$prev_link->addClass('prev_link');
			$prev_link->setHref( '/news/view/' . $prev_post->id() );
			$prev_link->addText( $prev_post->title());

			$prev_next_div->attachView( $prev_link );
		}

		if ( $next_post ){
			$next_link = new TCv_Link();
			$next_link->addClass('next_link');
			$next_link->setHref( '/news/view/' . $next_post->id() );
			$next_link->addText( $next_post->title() );

			$prev_next_div->attachView( $next_link );
		}
		

		$this->attachView($prev_next_div);
		return parent::html();
	}


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////


	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
	
		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() : string { return 'MailChimp Post View'; }
	public static function pageContent_IconCode() : string { return 'fa-envelope'; }
	
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A view that loads a single post based on the ID provided in the URL';
	}
	
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false; // the view comes from the ID in the URL, we won't have anythign to show in the preview
	}
	
	public static function pageContent_View_InputModelName() : null|array|string
	{
		return 'TMm_MailChimpPost'; // This view requires an instance of a TMm_MailChimpPost, with the ID passed via the URL string
	}
}
?>