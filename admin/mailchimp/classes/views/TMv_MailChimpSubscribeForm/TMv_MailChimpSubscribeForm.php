<?php
/**
 * Class TMv_MailChimpSubscribeForm
 *
 * A form that allows a person to subscribe to a particular list
 */
class TMv_MailChimpSubscribeForm extends TCv_Form
{
	use TMt_PagesContentView;
	
	protected $list_id = false;
	protected $submit_button_text = '';
	protected $show_name_fields = true;

	protected static $response_subscribed = 'You are already subscribed. Thank you!';
	protected static $response_pending = 'Your subscription is pending confirmation. Please check your email.';
	protected static $response_added = 'You have been successfully subscribed.';
	protected static $response_error = 'There was an error adding your subscription';
	
	/**
	 * TMv_MailChimpSubscribeForm constructor.
	 * @param string $id
	 */
	public function __construct($id = 'mailchimp_signup_form')
	{	
		parent::__construct($id);
		
	}
	
	/**
	 * Sets the list ID that the form subscribes to
	 * @param string $list_id
	 */
	public function setListID($list_id)
	{	
		$this->list_id = $list_id;
		
	}
	
	/**
	 * Configures the form elements
	 */
	public function configureFormElements()
	{
		if($this->submit_button_text != '')
		{
			$this->setButtonText($this->submit_button_text);
		}

		if($this->show_name_fields)
		{
			$first_name = new TCv_FormItem_TextField('first_name', 'First Name');
			$first_name->setPlaceholderText('First Name');
			$first_name->setIsRequired();
			$this->attachView($first_name);

			$last_name = new TCv_FormItem_TextField('last_name', 'Last Name');
			$last_name->setPlaceholderText('Last Name');
			$last_name->setIsRequired();
			$this->attachView($last_name);
		}

		$email = new TCv_FormItem_TextField('email', 'Email');
		$email->setPlaceholderText('email@address.com');
		$email->setIsEmail();
		$email->setIsRequired();
		$email->disableAutoComplete();
		$this->attachView($email);
		
		$list_id = new TCv_FormItem_Hidden('list_id', 'List ID');
		$list_id->setValue($this->list_id);
		$this->attachView($list_id);
		
	}
	
	/**
	 * Returns the html for the form
	 * @param bool $show_form
	 * @return string
	 */
	public function html($show_form = true)
	{
		// CHECK FOR A VALID LIST
		if($this->list_id === false || $this->list_id == '')
		{
			$this->list_id = TC_getModuleConfig('mailchimp', 'default_list_id');
		}
		
		// check again in case the default_list_id isn't set yet
		if($this->list_id === false || $this->list_id == '')
		{
			return 'ERROR: List ID must be sent for Subscribe Forms';
		}
		
		return parent::html();
	}
	
	
	/**
	 * Override to not perform the primary DB action. None is necessary
	 * @param TCc_FormProcessor $form_processor
	 * @return bool
	 */
	public static function customFormProcessor_performPrimaryDatabaseAction($form_processor)
	{
		return false;
	}
	
	/**
	 * Method that performs the API calls to subscribe a user
	 * @param TCc_FormProcessor $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{

		// SETUP THE API CALLS
		$mailchimp = new TMm_MailChimp();
		
		// Save Field values for convenience
		$email = $form_processor->formValue('email');

		// Deal with optional names
		$first_name = '';
		$last_name = '';
		if($form_processor->fieldIsSet('first_name'))
		{
			$first_name = $form_processor->formValue('first_name');
			$last_name = $form_processor->formValue('last_name');
		}


		$list_id = $form_processor->formValue('list_id');

		// Get subscriber status
		// false indicates they aren't on the list yet, any other value indicates they are already a subscrbier
		$status = $mailchimp->subscriberStatusForList($email, $list_id);
		if($status == 'subscribed')
		{
			TC_message(static::$response_subscribed);
		}
		elseif($status == 'pending')
		{
			TC_message(static::$response_pending);
		}
		else // NOT in the list yet
		{
			$form_processor->addConsoleDebug('Sending new person to MailChimp');
			$method = '/lists/'.$list_id.'/members/';
			$arguments = array(
				'email_address' 	=> $email,
				'status' 			=> 'subscribed'
				
			);

			if($form_processor->fieldIsSet('first_name'))
			{
				$arguments['merge_fields'] = array('FNAME' => $first_name, 'LNAME' => $last_name);
			}

			$response = $mailchimp->post($method, $arguments);
			if($response['status'] == 'subscribed')
			{
				TC_message(static::$response_added);
			}
			else
			{
				TC_message(static::$response_error, false);
				TC_message($response['detail']);
			}
			
		}
		
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		$select_options= array();
		$mailchimp = TMm_MailChimp::init();
		$lists = $mailchimp->lists();

		foreach ($lists as $index => $list_values)
		{
			$select_options[$list_values['id']] = $list_values['name'];
		}

		$select = new TCv_FormItem_Select( 'list_id', 'MailChimp List' );
		$select->setHelpText( 'Sync Mailchimp module to update lists' );
		$select->addOptions( $select_options );
		$form_items[] = $select;


		$select = new TCv_FormItem_Select( 'show_name_fields', 'Show Name Fields' );
		$select->setHelpText( 'Indicate if the name fields are shown' );
		$select->addOption( '1', 'Yes – Ask for the first and last name' );
		$select->addOption( '0', 'No – Just ask for the email address' );
		$form_items[] = $select;

		$button_text = new TCv_FormItem_TextField( 'submit_button_text', 'Button Text' );
		$button_text->setHelpText( 'Indicate what the button should say' );
		$button_text->setDefaultValue('Sign Up For Our Mailing List');
		$form_items[] = $button_text;


		return $form_items;
	}


	public static function customFormProcessor_skipAuthentication()
	{
		return true;
	}


	public static function pageContent_ViewTitle() : string { return 'MailChimp Subscribe Form'; }
	public static function pageContent_IconCode() : string { return 'fa-envelope'; }
	public static function pageContent_ShowPreviewInBuilder() : bool
	{
		return false;
	}
	public static function pageContent_ViewDescription() : string
	{ 
		return 'A subscription form for MailChimp that is associated with a single list.';
	}
	
	
}
?>