<?php
class TMv_MailChimpUserForm extends TMv_UserForm
{
	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		/** @var TMm_MailChimp $mailchimp */
		$mailchimp = TC_initClass('TMm_MailChimp');

		foreach($mailchimp->lists() as $list_properties)
		{
			$current_status = $mailchimp->subscriberStatusForList($this->model()->email(), $list_properties['id']);
			$current_status = ucwords($current_status);
			$is_subscribed = $current_status != '';
			if(!$is_subscribed)
			{
				$current_status = 'Not Subscribed';
			}

			$field = new TCv_FormItem_HTML($list_properties['id'].'_status', $list_properties['name']);
			$field->addText('<p>'.$current_status.'</p>');

			if(!$is_subscribed)
			{
				$add_button = new TCv_Link();
				$add_button->addText('Add to MailChimp List');
				$add_button->setURL('/admin/mailchimp/do/add-user-to-default-list/'.$this->model()->id());

				$p = new TCv_View();
				$p->setTag('p');
				$p->attachView($add_button);
				$field->attachView($p);
			}

			$this->attachView($field);



		}

		$this->hideSubmitButton();


	}
}