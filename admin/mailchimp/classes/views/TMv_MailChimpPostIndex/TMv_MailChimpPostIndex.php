<?php
/*	CLASS		:	TMv_MailChimpPostList
	DESCRIPTION	:	This is the list of posts in our system. 
	ADDED		:	
 */
class TMv_MailChimpPostIndex extends TCv_View
{
	//use TMt_PagesContentView;


	protected $mailchimp = null;
	protected $res_per_page = 10;
	protected $current_page = 1;
	protected $mailchimp_list_id;

	/* 	FUNCTION	: 	__construct
		DESCRIPTION	:	Constructor
		PARAMS		:	
		RESULT		:	
		ADDED		:	
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addClassCSSFile('TMv_MailChimpPostIndex');
		$this->mailchimp = TMm_MailChimp::init(false);

	}

	public function html()
	{

		$variables = $this->content_item->variables();

		if ( $variables['paginated'] == '1' && isset($_GET['p'] ) && is_numeric( $_GET['p'] ) )
		{
			$this->current_page = $_GET['p'];
		}

		if ( isset( $variables['per_page'] ) && $variables['per_page'] > 0 ) 
		{
			$this->res_per_page = $variables['per_page'];
		} 

		$mailchimp_list_id = $variables['mailchimp_list_id'];
		
		$postsList = $this->mailchimp->getPostsList( $this->mailchimp_list_id, $this->res_per_page, $this->current_page );	

		$show_excerpt = ( isset( $variables['show_excerpt'] ) && $variables['show_excerpt'] === '1' );

		if ( $postsList )
		{
			$ul = new TCv_View();
			$ul->setTag('ul');

			foreach( $postsList as $post )
			{
				$link_url = '/news/view/' . $post->id();
				$a = new TCv_View();
				$a->setTag('a');
				$a->setAttribute('href', $link_url);

				$li = new TCv_View();
				$li->setTag('li');

				$title = new TCv_View();
				$title->setTag('h3');
				$title->addText( $post->title() );

				$a->attachView($title);
				$li->attachView($a);

				if ( $show_excerpt ) 
				{
					$p = new TCv_View();
					$p->setTag('p');
					$p->addText( $post->excerpt() );
					$li->attachView($p);
				}

				$ul->attachView($li);
			}

			$this->attachView($ul);
		}

		if ( $variables['paginated'] == 1 )
		{
			$this->attachView( $this->paginationComponent() );	
		}
		
		return parent::html();
	}

	public function pageContent_EditorFormItems() : array
	{
		$form_items = array();
		$select_options = array();

		$mailchimp = TMm_MailChimp::init(false);
		$lists = $mailchimp->getLists();

		foreach ($lists as $list) 
		{
			$select_options[$list->id()] = $list->name();
		}
		$select = new TCv_FormItem_Select( 'mailchimp_list_id', 'Select List' );
		$select->setHelpText( 'Sync Mailchimp module to update lists' );
		$select->setOptionsWithArray( $select_options );
		$form_items[] = $select;

		$excerpt = new TCv_FormItem_Checkbox( 'show_excerpt', 'Show Excerpt' );
		$form_items[] = $excerpt;

		$per_page = new TCv_FormItem_TextField( 'per_page', 'Number of Posts' );
		$per_page->setHelpText( 'Default: 10' );
		$form_items[] = $per_page;

		$paginated = new TCv_FormItem_Checkbox( 'paginated', 'Paginate Results' );
		$form_items[] = $paginated;

		return $form_items;
	}

	public static function pageContent_ViewTitle() : string { return 'MailChimp Posts List (Simple)'; }
	public static function pageContent_IconCode() : string { return 'fa-envelope'; }
	public static function pageContent_ViewDescription() : string { return 'Displays a list of Mailchimp posts'; }
	public static function pageContent_ShowPreviewInBuilder() : bool { return false; }
	
	// TODO: make this tungsten 
	public function paginationComponent()
	{
		
		$total_res = $this->mailchimp->getPostsCount();

		$prev_next_div = new TCv_View('prev_next');

		if ( ( ($this->current_page - 1) * $this->res_per_page) > 0 )
		{
			$prev_link = new TCv_Link();
			$prev_link->addClass('prev_link');
			$prev_link->setHref( '/news/?p=' . ($this->current_page - 1) );
			$prev_link->addText( '&lsaquo; Prev');
			$prev_next_div->attachView( $prev_link );
		}

		if ( $this->current_page * $this->res_per_page < $total_res )
		{
			$next_link = new TCv_Link();
			$next_link->addClass('next_link');
			$next_link->setHref( '/news/?p=' . ($this->current_page + 1) );
			$next_link->addText( 'Next &rsaquo;');
			$prev_next_div->attachView( $next_link );
		}

		return $prev_next_div;
	}
}
?>