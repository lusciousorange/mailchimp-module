<?php

class TMv_MailChimpPostPreview extends TCv_View
{

	protected $post = null;
	protected $show_excerpt = true;
	protected $view_url = '/newsletter/view/';

	/**
	 * TMv_MailChimpPostPreview constructor.
	 * @param TMm_MailChimpPost $mailchimp_post
	 */
	public function __construct($mailchimp_post)
	{
		parent::__construct('mailchimp_preview_'.$mailchimp_post->id());

		$this->post = $mailchimp_post;
	}

	/**
	 * Turns off showing the excerpt
	 */
	public function hideExcerpt()
	{
		$this->show_excerpt = false;
	}
	
	/**
	 * Turns on showing the excerpt. It is "on" by default so this isn't necessary to call.
	 */
	public function showExcerpt()
	{
		$this->show_excerpt = true;
	}

	/**
	 * Sets the URL that will be used to to link for this post
	 * @param string $url
	 */
	public function setViewURL($url)
	{
		$this->view_url = $url;
	}

	public function html()
	{
		$this->addConsoleDebug("Creating Post");
		// Create a heading and put hte link inside of it
		$heading = new TCv_View();
		$heading->setTag('h3');
			
			$link = new TCv_Link();
			$link->setURL($this->view_url. $this->post->id());
			$link->addText($this->post->title());
			$link->addClass('title_link');
			$heading->attachView($link);
		$this->attachView($heading);
		
		$date = new TCv_View();
		$date->addClass('post_date');
		$date->addText($this->post->dateSentFormatted());
		$this->attachView($date);
		
		// If there's an excerpt to be shown, do that too
		if($this->show_excerpt)
		{
			$p = new TCv_View();
			$p->setTag('p');
			$p->addText( $this->post->excerpt() );
			$this->attachView($p);
		}			
		return parent::html();
	}



}
?>